#pragma once
#ifndef O3D_BS_TREE_H
#define O3D_BS_TREE_H



template<typename T>
class BSTree {

public:
	T val_;
	BSTree* leftNode_ = nullptr;
	BSTree* rithrNode_ = nullptr;
	void add(BSTree& it) {
		this->add(&it);
	}
	void add(BSTree* it) {
		if (!leftNode_ && it->val_ <= this->val_) {
			leftNode_ = it;
			return;
		}
		if (!rithrNode_ && it->val_ > this->val_) {
			rithrNode_ = it;
			return;
		}
		if (it->val_ <= this->val_) {
			this->leftNode_->add(it);
		} else {
			this->rithrNode_->add(it);
		}
	}
};


template<>
class BSTree<unsigned char> {
public:

	BSTree<unsigned char>(const uint8_t& ival) :val_(ival) {

	}
	BSTree<unsigned char>() : val_(0) {

	}


	unsigned char val_;
	BSTree <unsigned char>* leftNode_ = nullptr;
	BSTree <unsigned char>* rithrNode_ = nullptr;
	void add(BSTree <unsigned char>& it) {
		this->add(&it);
	}
	void add(BSTree <unsigned char>* it) {
		if (!leftNode_ && it->val_ <= this->val_) {
			leftNode_ = it;
			return;
		}
		if (!rithrNode_ && it->val_ > this->val_) {
			rithrNode_ = it;
			return;
		}
		if (it->val_ <= this->val_) {
			this->leftNode_->add(it);
		} else {
			this->rithrNode_->add(it);
		}
	}
};




#endif