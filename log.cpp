#include "log.h"
o3d::utility::logger& o3d::utility::logger::GetLogger() {
	static logger lg; // 不在头文件中写静态成员.
	return lg;
}
std::string o3d::utility::logger::GetTime() {
	time_t ticks = time(NULL);
	struct tm* ptm = new tm;
	int re = localtime_s(ptm, &ticks);
	if (re != 0) {
		delete ptm;
		return "";
	}
	char timestamp[32];
	for (size_t i = 0; i < 32; i++) {
		timestamp[i] = '\0';
	}
	strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%m:%S", ptm);
	delete ptm;
	return timestamp;
}

void o3d::utility::logger::logToFile(std::stringstream& info) {
	mtxFile_.lock();
	logOfsteam_ << info.str();
	mtxFile_.unlock();
}

void o3d::utility::logger::logToScreen(std::stringstream& info) {
	mtxScreen_.lock();
	std::cout << info.str();
	mtxScreen_.unlock();
}

o3d::utility::logger::logger() {
	if (this->isWriteFile_) {
		this->open();
	}
}

void o3d::utility::logger::open() {
	logOfsteam_.open(logFileName_, std::ios::app);
	if (!logOfsteam_.is_open()) {
		throw "not open log file";
		return;
	}
	return;
}

void o3d::utility::logger::close() {
	mtxScreen_.lock();
	mtxFile_.lock();
	if (this->isWriteFile_ && this->logOfsteam_.is_open()) {
		logOfsteam_ << std::flush;
		logOfsteam_.close();
	}
	mtxFile_.unlock();
	mtxScreen_.unlock();
}