﻿#include <iostream>
#include<string>
#include"log.h"
using namespace o3d::utility;
#include<array>
#include<assert.h>
#include<cmath>
#include"array.h"
#include"matrix.h"
#include"bsTree.h"
#if 1

int tmain01() {
	using namespace o3d;
	typedef Matrix<double, 3, 3> Matx3d;
	typedef Matrix<double, 3, 1> V3d;
	//V3d v1{ 1,2,3 };
	//// 叉乘.

	//{
	//	Matx3d m1 = Matx3d::Identity();
	//	m1[0] = 3;
	//	Cross(m1, v1);
	//	LogInfo("\n", v1);
	//}
	//{
	//	v1 = { 1,2,3 };
	//	Matx3d m1 = Matx3d::Identity();
	//	m1[0] = 3;
	//	LogInfo("\n", m1.cross(v1));
	//}
	//{
	//	Matx3d m1 = { 1,2,3,0,1,0,0,0,1 };
	//	Matx3d m2 = { 1,2,3,0,1,0,0,0,1 };
	//	Cross(m1, m2);
	//	LogInfo("\n", m2);

	//}
	//{
	//	Matx3d m1 = { 1,2,3,0,1,0,0,0,1 };
	//	Matx3d m2 = { 1,2,3,0,1,0,0,0,1 };
	//	LogInfo("\n", m1.cross(m2));
	//}
	//{
	//	Matx3d m1 = { 1,2,3,0,1,0,0,0,1 };
	//	Matx3d m2 = { 1,2,3,0,1,0,0,0,1 };
	//	m1.crossed(m2);
	//	LogInfo("\n", m1);
	//}

	return 0;
}
#include<sstream>







int tmain02() {
	using namespace o3d;
	typedef Matrix<double, 3, 3> Matx3d;
	typedef Matrix<double, 3, 1> V3d;
	V3d v1{ 3,4,1 };


	Matx3d m1 = Matx3d::Identity();


	// m1.block<u1, u2>(u3, u3);
	// Matx3d::Black<3, 1> b1(m1, 0, 0);
	m1.block<3, 1>(0, 0) = v1;

	Matrix<double, 3, 1> m2 = (m1.block<3, 1>(0, 0));


	LogInfo(m1);
	LogInfo(v1);

	return 0;
}




#include<vector>
int tmain03() {
	std::vector<BSTree<uint8_t>> v1;
	for (size_t i = 0; i < 10; i++) {

	}
	BSTree<uint8_t> list1;
	list1.val_ = 12;

	return 0;
}
void computeMeanAndVariance(std::vector<float>v1, float& mean, float* variance) {
	double _mean = 0.0; // 数组元素的和.
	double _std2 = 0.0; // 数组元素平方和.
	std::size_t count = 0;

	for (std::size_t i = 0; i < v1.size(); ++i) {
		const float& val = v1.at(i);
		if ((val == val)) // 这里直接放弃 了空元素.
		{
			_mean += val;
			_std2 += static_cast<double>(val) * val;
			++count;
		}
	}

	if (count) {
		_mean /= count; // 平均数.
		mean = static_cast<float>(_mean); // 返回的平均数.

		if (variance) {
			_std2 = std::abs(_std2 / count - _mean * _mean);
			*variance = static_cast<float>(_std2);
		}
	} else {
		mean = 0;
		if (variance) {
			*variance = 0;
		}
	}
}

int tmain04() {
	std::vector<float>v1 = { 5,3,9,0,7,4,1,6,3,2 };
	float sum1 = 0;
	for (size_t i = 0; i < v1.size(); i++) {
		sum1 += v1.at(i);
	}
	float sum2 = 0;
	float va2 = 0;
	computeMeanAndVariance(v1, sum2, &va2);

	double d1 = NAN;
	double d2 = NAN;
	double d3 = 10;
	double d4 = 10;

	LogInfo(d1 == d2);
	LogInfo(d3 == d4);

	return 0;
}

o3d::Matrix<double, 3, 1> t_fun(o3d::Matrix<double, 3, 3>& m, const o3d::Matrix<double, 3, 1>& v) {
	o3d::Matrix<double, 3, 1> mout = { 0,0,0 };
	// 计算矩阵乘积.
	//mout.x() = m(0, 0) * v.x() + m(0, 1) * v.y() + m(0, 2) * v.z();
	//mout.y() = m(1, 0) * v.x() + m(1, 1) * v.y() + m(1, 2) * v.z();
	//mout.z() = m(2, 0) * v.x() + m(2, 1) * v.y() + m(2, 2) * v.z();
	mout = m * v;
	mout.x() /= mout.z();
	mout.y() /= mout.z();
	mout.z() /= mout.z();
	return mout;
}
int tmain05() {
	using namespace o3d;
	typedef Matrix<double, 3, 3> Matx3d;
	typedef Matrix<double, 3, 1> V3d;
	// 假设是一个常规的的坐标系.观测点在(0,0) n = 10 f = 20 过近点(10,10).

	Matx3d m = Matx3d::Zero();
	V3d v1 = { 15,15,1 };
	V3d v2 = { 15,15,1 };
	// 设置m矩阵.
	m(0, 0) = 10;
	m(2, 1) = 1;
	m(1, 1) = 30;
	m(1, 2) = -200;
	
	LogInfo(m);
	LogInfo("--------------- y = x");
	for (double i = 10; i < 21; i++) {
		v1 = { i,i,1 };
		v2 = t_fun(m, v1);
		LogInfo("y = ", v1.y(), "    y2-y1 = ", v2.y() - v1.y());
		//	INFO  y = 10    y2 - y1 = 0
		//	INFO  y = 11    y2 - y1 = 0.818182
		//	INFO  y = 12    y2 - y1 = 1.33333
		//	INFO  y = 13    y2 - y1 = 1.61538
		//	INFO  y = 14    y2 - y1 = 1.71429
		//	INFO  y = 15    y2 - y1 = 1.66667
		//	INFO  y = 16    y2 - y1 = 1.5
		//	INFO  y = 17    y2 - y1 = 1.23529
		//	INFO  y = 18    y2 - y1 = 0.888889
		//	INFO  y = 19    y2 - y1 = 0.473684
		//	INFO  y = 20    y2 - y1 = 0
		// 这样看的画所有的y值相对原来的y值增加了
	}
	LogInfo("--------------- x = 2y");
	for (double i = 10; i < 21; i++) {
		v1 = { i*2,i,1 };
		v2 = t_fun(m, v1);
		LogInfo(v1.transposition(),"   ",v2.transposition(), "    y2-y1 = ", v2.y() - v1.y());
		//  INFO{ 20,10,1 }   {20, 10, 1}    y2 - y1 = 0
		//	INFO{ 22,11,1 }   {20, 11.8182, 1}    y2 - y1 = 0.818182
		//	INFO{ 24,12,1 }   {20, 13.3333, 1}    y2 - y1 = 1.33333
		//	INFO{ 26,13,1 }   {20, 14.6154, 1}    y2 - y1 = 1.61538
		//	INFO{ 28,14,1 }   {20, 15.7143, 1}    y2 - y1 = 1.71429
		//	INFO{ 30,15,1 }   {20, 16.6667, 1}    y2 - y1 = 1.66667
		//	INFO{ 32,16,1 }   {20, 17.5, 1}    y2 - y1 = 1.5
		//	INFO{ 34,17,1 }   {20, 18.2353, 1}    y2 - y1 = 1.23529
		//	INFO{ 36,18,1 }   {20, 18.8889, 1}    y2 - y1 = 0.888889
		//	INFO{ 38,19,1 }   {20, 19.4737, 1}    y2 - y1 = 0.473684
		//	INFO{ 40,20,1 }   {20, 20, 1}    y2 - y1 = 0
	}
	return 0;
}


o3d::Matrix<double, 3, 1> Str2V3d(std::string istr) {
	std::string st1 = istr; // 前一个数字.
	size_t i = 0;
	for (i = 0; i < st1.size(); i++) {
		if (st1.at(i) == ' ') {
			break;
		}
	}
	if (i == st1.size()) {
		// throw std::runtime_error("not have vector");
		return { 0,0,0 };
	}
	st1 = st1.substr(0, i);
	istr = istr.substr(i+1);
	return { stod(st1),stod(istr),1};
}
int tmain06() {
	using namespace o3d;
	typedef Matrix<double, 3, 3> Matx3d;
	typedef Matrix<double, 3, 1> V3d;

	std::string st1 = "123 789";

	Matx3d m = Matx3d::Zero();
	V3d v1 = { 0,10,1 };
	V3d v2 = { 0,0,0 };
	// 设置m矩阵.
	m(0, 0) = 10;
	m(2, 1) = 1;
	m(1, 1) = 30;
	m(1, 2) = -200;

	while (v1.z()!=0) {
		v2 = t_fun(m, v1);
		LogDebug("\n(", v1.y(),",",v1.x() ,")\n(", v2.y(),",",v2.x(),")");
		LogInfo("输入数组:  例子 10 20");
		std::getline(std::cin, st1);
		v1 = Str2V3d(st1);
	}






	return 0;
}
int main(int argc, char* argv[]) {




	tmain06();
	// tmain03();
	return 0;
}
#endif