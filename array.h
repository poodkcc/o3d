#pragma once
#ifndef O3D_ARRAY_H
#define O3D_ARRAY_H
#include<stdint.h>
namespace o3d {
template<typename T, uint64_t arrayLen>
class array {
public:
	T data_[arrayLen];
	operator T* () {
		return data_;
	}
	inline constexpr const uint64_t size()const {
		return arrayLen;
	}
	inline static array<T, arrayLen>Zero() {
		return array<T, arrayLen>({ 0 });
	}
	T& operator[](const uint64_t& index) {
		return data_[index];
	}
};
}
template<typename T>
class vector {
public:
	vector(std::initializer_list<T> ilist) {
		data_ = new T[ilist.size()];
		T* it1 = data_;
		auto it2 = ilist.begin();
		for (size_t i = 0; i < ilist.size(); i++) {
			*it1 = *it2;
			++it1;
			++it2;
		}
		this->end_ = it1;
		this->containerEnd_ = it1;
	}

	~vector() {
		if (data_) {
			delete[] data_;	data_ = nullptr;
		}
	}
	uint64_t capacity() {
		if (data_) {
			return static_cast<uint64_t>(containerEnd_ - data_);

		}
		return 0;
	}
	uint64_t size() {
		if (data_) {
			return static_cast<uint64_t>(end_ - data_);
		}
		return 0;
	}

	T* data_;
	T* end_;
	T* containerEnd_;
};

#endif