﻿#pragma once
#ifndef O3D_LOG_H
#define O3D_LOG_H
#include <iostream>
#include <mutex>
#include<fstream>
#include<sstream>
#include<string>

namespace o3d {
namespace utility {

#define LogDebug(...)							\
    o3d::utility::logger::GetLogger().logDebug(__FILE__,__FUNCTION__, __LINE__,__VA_ARGS__)
#define LogInfo(...)							\
    o3d::utility::logger::GetLogger().logInfo(__VA_ARGS__)
#define LogError(...)							\
    o3d::utility::logger::GetLogger().logErr(__FILE__,__FUNCTION__, __LINE__,__VA_ARGS__)
class logger {
	enum Level :uint8_t {
		DEBUG = 0,
		INFO,
		ERROR,
		FATAL
	};
public:
	static logger& GetLogger();
public:

	template<typename... Args>
	void logDebug(const std::string& ifileName, const std::string& ifunName, const uint32_t iline, Args&&...args);

	template<typename... Args>
	void logInfo(Args&&...args);

	template<typename... Args>
	void logErr(const std::string& ifileName, const std::string& ifunName, const uint32_t iline, Args&&...args);

	static std::string GetTime();

	inline ~logger() { this->close(); }
private:
	logger();
	void open();
	void close();
	void logToFile(std::stringstream& info);
	void logToScreen(std::stringstream& info);
	template<typename T>
	std::stringstream& logWriteStem(std::stringstream& istm, const T& igs);
	template<typename T, typename... Args>
	std::stringstream& logWriteStem(std::stringstream& istm, const T& igs, Args&&...args);
private:
	bool isWriteFile_ = false;
	bool isWriteTime_ = false;
	bool isFileName_ = false;
	const char* logFileName_ = "test.log";
	std::ofstream logOfsteam_;
	std::mutex mtxScreen_;
	std::mutex mtxFile_;
	const char* LeverStr[Level::FATAL] = { "DEBUG","INFO ","ERROR" };
};

template<typename T>
inline std::stringstream& logger::logWriteStem(std::stringstream& istm, const T& igs) {
	istm << igs << '\n';
	return istm;
}

template<typename T, typename ...Args>
inline std::stringstream& logger::logWriteStem(std::stringstream& istm, const T& igs, Args && ...args) {
	istm << igs;
	logWriteStem(istm, args...);
	return istm;
}

template<typename ...Args>
inline void logger::logDebug(const std::string& ifileName, const std::string& ifunName, const uint32_t iline, Args && ...args) {
#ifdef _DEBUG
	std::stringstream istm;
	istm << LeverStr[Level::DEBUG];
	if (isWriteTime_) {
		istm << ' ' << GetTime();
	}
	if (isFileName_) {
		istm << ' ' << ifileName;
	}
	istm << ' ' << ifunName << ':' << iline << ' ';
	logWriteStem(istm, args...);
	logToScreen(istm);
	if (isWriteFile_) {
		logToFile(istm);
	}
#endif // _DEBUG
}

template<typename ...Args>
inline void logger::logInfo(Args && ...args) {
	std::stringstream istm;
	istm << LeverStr[Level::INFO] << ' ';
	logWriteStem(istm, args...);
	logToScreen(istm);
	if (isWriteFile_) {
		logToFile(istm);
	}
}

template<typename ...Args>
inline void logger::logErr(const std::string& ifileName, const std::string& ifunName, const uint32_t iline, Args && ...args) {
	std::stringstream istm;
	istm << LeverStr[Level::ERROR];
	if (isWriteTime_) {
		istm << ' ' << GetTime();
	}
	if (isFileName_) {
		istm << ' ' << ifileName;
	}
	istm << ' ' << ifunName << ':' << iline << ' ';
	logWriteStem(istm, args...);
	logToScreen(istm);
	if (isWriteFile_) {
		logToFile(istm);
	}
}
}
}
#endif  // !ULOG_H.
